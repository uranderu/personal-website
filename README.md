# My Personal Website

Source code for my personal website that is publicly available at https://www.fab1an.dev

### Goals

I'm still learning a lot about web development, this site will evolve over time as I get more proficient in this field.
I'll create a release before each "big" change.

### Current Release

Currently, we're at V4.0 of this website. This release switches the framework from SvelteKit to Astro.

#### Why?

- I wanted to learn Astro
- SvelteKit will always ship the "runtime". Even though you might not use some or all of its functions. Astro ships zero
  JavaScript by default. Making the website even faster.
