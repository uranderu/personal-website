import eslintPluginAstro from 'eslint-plugin-astro';

export default [
  ...eslintPluginAstro.configs.recommended,
  {
    rules: {},
    ignores: [
      'dist/**',
      '.astro/**',
      'node_modules/**',
      '**/npm-debug.log*',
      '.env*',
      '.DS_Store',
      '.idea/**',
      'test-results/**',
      'playwright-report/**',
      'playwright/.cache/**',
    ],
  },
];
