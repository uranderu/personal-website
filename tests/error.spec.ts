import { expect, test } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('/error');
});

test('Expect error page with content', async ({ page }) => {
  const heading = await page.locator('h1').innerText();

  expect(heading).toMatch('Oops, something went wrong.');
});
