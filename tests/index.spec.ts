import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test('Expect navbar', async ({ page }) => {
  const navbar = await page.waitForSelector('nav');
  expect(navbar);
});

test('Expect hero', async ({ page }) => {
  const hero = await page.waitForSelector('#hero');
  expect(hero);
});

test('Expect skill section', async ({ page }) => {
  const skillSection = await page.waitForSelector('#skill-section');
  expect(skillSection);
});

test('Expect projects', async ({ page }) => {
  const projects = await page.waitForSelector('#projects');
  expect(projects);
});

test('Expect contact section', async ({ page }) => {
  const contact = await page.waitForSelector('#contact');
  expect(contact);
});

test('Expect footer', async ({ page }) => {
  const footer = await page.waitForSelector('footer');
  expect(footer);
});
