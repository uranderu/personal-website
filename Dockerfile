# Building stage
FROM node:22-alpine AS build
# Copy files
WORKDIR /app
COPY . ./
# Actually build the webstie
RUN npm run build

# Transfer generated files to NGINX (webserver)
FROM nginxinc/nginx-unprivileged:alpine
# Copy build files to right directory so that NGINX can serve them.
COPY --from=build /app/dist /usr/share/nginx/html
# Copy custom NGINX config
COPY nginx.conf /etc/nginx/conf.d/default.conf
